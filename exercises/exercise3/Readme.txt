================================================================================
=============   Physically-based Simulation in Computer Graphics   =============
================================================================================

There are three different ways to build this framework:

Visual Studio project (Windows)
===============================
The Visual Studio project can be directly opened and compiled. For VS version
newer than 2008, VS will first perform a conversion.

Makefile (Linux, untested on OSX)
=================================
On Linux, you should be able to build the project by calling 'make'. Be aware
that you need to have the necessary libraries installed, in this case OpenGL
(with GLU), GLUT and libpng.
For OSX, change the Makefile by commenting/uncommenting the marked lines.

CMake (experimental, all platforms)
===================================
You should be able to create platform-independent makefiles by using CMake.
Either run it using a GUI or the appropriate generateBuild* file. These files
can also be adjusted to generate makefiles for different compilers.
On Linux, you might need to install these additional libraries:
  freeglut-dev, libXi-dev, libXmu-dev
