#include "gauss_seidel.h"
#include "fluid2d.h"
#include <cmath>
#include <iostream>
#include <array>
#include <algorithm>

#define F_IDX(x,y) ((x) + (y) * _xRes)

// Problem 1
void ExSolvePoisson(int _xRes, int _yRes, int _iterations, double _accuracy, double* _field, double* _b)
{

	//initial guess for _field: set it to 0
	for (int y = 1; y < _yRes - 1; y++) {
		for (int x = 1; x < _xRes - 1; x++) {
			_field[F_IDX(x,y)] = 0;
		}
	}

	double error_square = 0;
	double residual = 2;

	for (int it = 0; it < _iterations; ++it) {

		error_square = 0;
		residual = 0;

		for (int y = 1; y < _yRes - 1; y++) {
			for (int x = 1; x < _xRes - 1; x++) {
				_field[F_IDX(x,y)] = (_field[F_IDX(x,y-1)] + _field[F_IDX(x,y+1)] + 
					_field[F_IDX(x-1,y)] + _field[F_IDX(x+1,y)] +_b[F_IDX(x,y)]) / 4;
			}
		}
		for (int y = 1; y < _yRes - 1; y++) {
			for (int x = 1; x < _xRes - 1; x++) {
				error_square += (_field[F_IDX(x,y)]-_b[F_IDX(x,y)])*(_field[F_IDX(x,y)]-_b[F_IDX(x,y)]);
			}
		}
		residual = sqrt(error_square);
		//For your debugging, and ours, please add these prints after every iteration
		if(it == _iterations - 1) 
			printf("Pressure solver: it=%d , res=%f \n", it, residual);
		if(residual < _accuracy)
			printf("Pressure solver: it=%d , res=%f, converged \n", it, residual);
		
	}

}

// Problem 2
void ExCorrectVelocities(int _xRes, int _yRes, double _dt, const double *_pressure, double *_xVelocity, double *_yVelocity)
{
	// Note: velocity u_{i+1/2} is practically stored at i+1
	double dx = 1. / _xRes;
	double dy = 1. / _yRes;
	double rho = 1.0;
	for (int y = 0; y < _yRes-1; y++) {
		for (int x = 0; x < _xRes-1; x++) {
			_xVelocity[F_IDX(x+1,y)] -= _dt/rho * (_pressure[F_IDX(x+1,y)] - _pressure[F_IDX(x,y)]) / dx;
			_yVelocity[F_IDX(x,y+1)] -= _dt/rho * (_pressure[F_IDX(x,y+1)] - _pressure[F_IDX(x,y)]) / dy;
		}
	}


}


// Problem 3
/**
 * _xRes,_yRes : resolution of the grid
 * _dt : time step
 * _xVelocity,_yVelocity : velocity buffer
 * _density : density buffer
 * _densityTemp : preallocated temporary density buffer
 * _xVelocityTemp,_yVelocityTemp : preallocated temporary velocity buffer
 */
void ExAdvectWithSemiLagrange(int _xRes, int _yRes, double _dt, double *_xVelocity, double *_yVelocity, double *_density, double *_densityTemp, double *_xVelocityTemp, double *_yVelocityTemp)
{

	using Vec2 = std::array<double,2>;
	using iVec2 = std::array<int,2>;
    // preset

    // for loop
    for (int y = 1; y < _yRes - 1 ; y++){
        for (int x = 1; x < _xRes - 1; x++) {
            // get current velocity
            iVec2 array_size{_xRes,_yRes};

            iVec2 pos{x,y};
            Vec2 veloc{0,0};
            veloc[0] += _xVelocity[F_IDX(x,y)] + _xVelocity[F_IDX(x+1,y)];
            veloc[1] += _yVelocity[F_IDX(x,y)] + _yVelocity[F_IDX(x,y+1)];
            
            for(int i : {0,1})
                veloc[i] *= 0.5*(array_size[i]-3);

			// back track to previous position
            Vec2 backtracked_position;
            iVec2 array_clamp_density{_xRes-2,_yRes-2};
            Vec2 array_clamp_veloc{_xRes-2.5,_yRes-2.5};
            iVec2 lower_index;
            iVec2 lower_index_half_x;
            iVec2 lower_index_half_y;
            Vec2 offset_in_cell;
            Vec2 offset_in_cell_half_x;
            Vec2 offset_in_cell_half_y;
			
            for(int i : {0,1}) {
                backtracked_position[i] = -_dt*veloc[i] + pos[i];
                // clamp
                backtracked_position[i] = std::max(backtracked_position[i],1.0);
                backtracked_position[i] = std::min(backtracked_position[i],(double)array_clamp_density[i]);
                // lower index density
                lower_index[i] = int(std::floor(backtracked_position[i]));
                offset_in_cell[i] = backtracked_position[i] - lower_index[i];
                // clamp
                backtracked_position[i] = std::max(backtracked_position[i],1.5);
                backtracked_position[i] = std::min(backtracked_position[i],array_clamp_veloc[i]);
                // lower index x-velocity
                lower_index_half_x[i] = int(std::floor(backtracked_position[i] - (not i)*0.5));
                offset_in_cell_half_x[i] = backtracked_position[i] - lower_index_half_x[i];
                // lower index y-velocity
                lower_index_half_y[i] = int(std::floor(backtracked_position[i] - i*0.5));
                offset_in_cell_half_y[i] = backtracked_position[i] - lower_index_half_y[i];
                // now clamp the coordinates
            }


            // bilinearly sample attributes
            // new x-velocity
            _xVelocityTemp[F_IDX(x,y)] =
                (1.0 - offset_in_cell_half_x[0])*(1.0 - offset_in_cell_half_x[1])*_xVelocity[F_IDX(lower_index_half_x[0],lower_index_half_x[1])] +
                (offset_in_cell_half_x[0])*(1.0 - offset_in_cell_half_x[1])*_xVelocity[F_IDX(lower_index_half_x[0]+1,lower_index_half_x[1])]+
                (1.0 - offset_in_cell_half_x[0])*(offset_in_cell_half_x[1])*_xVelocity[F_IDX(lower_index_half_x[0],lower_index_half_x[1]+1)]+
                (offset_in_cell_half_x[0])*(offset_in_cell_half_x[1])*_xVelocity[F_IDX(lower_index_half_x[0]+1,lower_index_half_x[1]+1)]
            ;
            // new y-velocity
            _yVelocityTemp[F_IDX(x,y)] =
                (1.0 - offset_in_cell_half_y[0])*(1.0 - offset_in_cell_half_y[1])*_yVelocity[F_IDX(lower_index_half_y[0],lower_index_half_y[1])] +
                (offset_in_cell_half_y[0])*(1.0 - offset_in_cell_half_y[1])*_yVelocity[F_IDX(lower_index_half_y[0]+1,lower_index_half_y[1])]+
                (1.0 - offset_in_cell_half_y[0])*(offset_in_cell_half_y[1])*_yVelocity[F_IDX(lower_index_half_y[0],lower_index_half_y[1]+1)]+
                (offset_in_cell_half_y[0])*(offset_in_cell_half_y[1])*_yVelocity[F_IDX(lower_index_half_y[0]+1,lower_index_half_y[1]+1)]
            ;
            // new density
            _densityTemp[F_IDX(x,y)] =
                (1.0 - offset_in_cell[0])*(1.0 - offset_in_cell[1])*_density[F_IDX(lower_index[0],lower_index[1])] +
                (offset_in_cell[0])*(1.0 - offset_in_cell[1])*_density[F_IDX(lower_index[0]+1,lower_index[1])]+
                (1.0 - offset_in_cell[0])*(offset_in_cell[1])*_density[F_IDX(lower_index[0],lower_index[1]+1)]+
                (offset_in_cell[0])*(offset_in_cell[1])*_density[F_IDX(lower_index[0]+1,lower_index[1]+1)]
            ;


		}
        // Note: velocity u_{i+1/2} is practically stored at i+1
		// what does the above comment refer to exactly ?
    }
    // swap everything
    for (int y = 1; y < _yRes-1; y++){
        for (int x = 1; x < _xRes-1; x++){
            _density[F_IDX(x,y)] = _densityTemp[F_IDX(x,y)];
            _xVelocity[F_IDX(x,y)] = _xVelocityTemp[F_IDX(x,y)];
            _yVelocity[F_IDX(x,y)] = _yVelocityTemp[F_IDX(x,y)];
        }
    }
}
