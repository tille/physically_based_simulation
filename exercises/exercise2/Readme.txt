================================================================================
=============   Physically-based Simulation in Computer Graphics   =============
================================================================================

This framework contains an experimental CMake file to create platform-independent
makefiles. It can be run using a GUI or the appropriate generateBuild* file.
These files can also be adjusted to generate makefiles for different compilers.
