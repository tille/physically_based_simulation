/*=====================================================================================*/
/*! \file		FEMElementTri.cpp
	\author		peterkau
	\brief		Implementation of class FEMElementTri
 */
/*=====================================================================================*/

#include "SimpleFEMDefs.h"
#include "FEMElementTri.h"
#include "FEMMesh.h"
#include <array>
#include <algorithm>

// TASK 3
void FEMElementTri::Assemble(FEMMesh *pMesh) const
{
	std::array<Vector2,3> grads;
	for(int j = 0; j < 3;++j) {
		computeSingleBasisDerivGlobalLES(j, grads[j], pMesh);
	}

	float area = 0.f;
	computeElementArea(pMesh,area);

	for (int i=0; i<GetNumElementNodes(); ++i) { // i \in {0, 1, 2}
		for (int j=0; j<GetNumElementNodes(); ++j) { // j \in {0, 1, 2}
			size_t global_idx_i = this->GetGlobalNodeForElementNode(i);
			size_t global_idx_j = this->GetGlobalNodeForElementNode(j);

			if (global_idx_i >= global_idx_j) {
				double value = area * (grads[i].x() * grads[j].x() + grads[i].y() * grads[j].y());
				pMesh->AddToStiffnessMatrix(global_idx_i,global_idx_j, value);
			}
		}
	}
}

// TASK 2
void FEMElementTri::computeSingleBasisDerivGlobalGeom(size_t local_index, Vector2 &basisDerivGlobal, const FEMMesh *pMesh) const
{
	// get position of all nodes
	std::array<Vector2, 3> a;
	for (unsigned i=0; i<GetNumElementNodes(); ++i) {
		a[i] = pMesh->GetNodePosition(m_nodes[i]);
	}

	// compute gradient
	basisDerivGlobal[0] = a[(local_index+1)%3][1]-a[(local_index+2)%3][1];
	basisDerivGlobal[1] = a[(local_index+2)%3][0]-a[(local_index+1)%3][0];

	float area = 0.f;
	computeElementArea(pMesh, area);
	basisDerivGlobal[0] /= 2 * area;
	basisDerivGlobal[1] /= 2 * area;
}

// TASK 1
void FEMElementTri::computeSingleBasisDerivGlobalLES(size_t local_index, Vector2 &basisDerivGlobal, const FEMMesh *pMesh) const
{

	// get position of all nodes
	std::array<Vector2, 3> a;
	for (unsigned i=0; i<GetNumElementNodes(); ++i) {
		a[i] = pMesh->GetNodePosition(m_nodes[i]);
	}

	Matrix3x3T<float> m;
	m(0, 0) = 1;
	m(1, 0) = 1;
	m(2, 0) = 1;
	for (unsigned i=0; i<3; ++i) {
		m(i, 1) = a[i][0];
		m(i, 2) = a[i][1];
	}

	auto m_inv = m.inverse(); // this matrix already contains all gradients for all local basis functions

	// compute gradient
	basisDerivGlobal[0] = m_inv(1, local_index);
	basisDerivGlobal[1] = m_inv(2, local_index);

}

void FEMElementTri::computeElementArea(const FEMMesh *pMesh, float &area) const {
	// get position of all nodes
	std::array<Vector2, 3> a;
	for (unsigned i=0; i<GetNumElementNodes(); ++i) {
		a[i] = pMesh->GetNodePosition(m_nodes[i]);
	}

	Matrix3x3T<float> m;
	m(0, 0) = 1;
	m(1, 0) = 1;
	m(2, 0) = 1;
	for (unsigned i=0; i<3; ++i) {
		m(i, 1) = a[i][0];
		m(i, 2) = a[i][1];
	}

	area = m.det()/2.;
}