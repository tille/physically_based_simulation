##### Ehrengruber Till, Schwarz Patrick, Sinner Eric 


# Physically-Based Simulation in Computer Graphics

## Report Exercise 1

###Problem 2

#### *2.1. Analytic solution*

The initial conditions for position and velocity are given by
$$y(0) = -L \quad , \quad \dot{y}(0) = 0$$

The functions $y(t)$ and $\dot{y}(t)$ are given by 
$$y(t) = c_1 e^ {\alpha t} cos(\beta t) + c_2 e^ {\alpha t} sin(\beta t) - L - \frac{mg}{k}$$
$$\dot{y}(t) = c_1 e^ {\alpha t} (\alpha cos(\beta t) - \beta sin(\beta t) ) + c_2 e^ {\alpha t} (\alpha sin(\beta t) + \beta cos(\beta t))$$

Using the initial conditions above we obtain
$$y(0) = c_1 -L -\frac{mg}{k} = -L  \quad \iff \quad c_1 = \frac{mg}{k}$$
$$\dot{y}(0) = c_1 \alpha  + c_2 \beta = 0 \quad \iff \quad c_2 = - \frac{mg}{k}\frac{\alpha}{\beta}$$

*For concrete values and data of the error convergence and stability analysis, please refer to the datafiles in the /results folder. The error convergence analysis has been written in julia in an IJulia Notebook (similar to an IPython Notebook). The notebook has been exported twice as html-files (calculations with and without damping).*

#### *2.2. Error convergence analysis*

Without damping, we observe first order convergence for the __explicit Euler, symplectic Euler and implicit Euler__ methods which means if the step size is halved, the error is four times smaller. 

For the __midpoint__ method, the error gets approximatly 8 times smaller if the step size is halved which is in accordance with theory where that very method is of second-order.

With damping ($damp = 0.5$) however, we observe for all 4 methods that if we halve the step size, the error only gets two times smaller, i.e. all the tested methods mentioned above become of order 0.

We conclude that the introduction of a damping force makes all methods significantly worse in terms of convergence rate towards the analytical solution for small step sizes.


#### *2.3. Stability analysis*

We see for all testcases that, apart from the analytical solution, the implicit Euler method is the only one whose amplitude is finite regardless of how big the step size is chosen. This confirms theory which says that the implicit Euler method is stable for all $dt > 0$.

On the other hand, we observe that the forward Euler method is in each case the worst method concerning stability as the values of the maximal amplitude tend rather quickly towards infinity as we make the step size bigger.

The midpoint and symplectic Euler method seems to be equally bad, but one notices slightly better results for the latter method.

We can only consider those methods to be accurate whose maximal amplitude remains in all cases finite as this constraint is imposed by the physical mass-spring-system.
