#include "Vec2.h"
#include <array>
#include <eigen3/Eigen/Dense>


// gravitational acceleration (9.81)
static const double g = 9.81;

using Vector2d = Eigen::Matrix<double, 2, 1>;
using Matrix2x2 = Eigen::Matrix<double, 2, 2>;

/** Exercise 1.1 - hanging mass point
 * \param k spring constant / stiffness
 * \param m mass
 * \param d damping
 * \param L spring length
 * \param dt time step
 * \param method integration method
 * \param p1 current position
 * \param v1 current velocity
 * \param p2 position at t+dt
 * \param v2 velocity at t+dt
 */
void AdvanceTimeStep1(double k, double m, double d, double L, double dt, 
  int method, double p1, double v1, double& p2, double& v2)
{
  Vector2d y(p2, v2);

  auto force = [&] (const Vector2d& y) {
    return k * (-L-y[0]) - m*g;
  };

  // y' = f(y)
  auto f = [&] (const Vector2d& y) {
    return Vector2d(y[1], (force(y)-d*y[1])/m);
  };

  switch (method) {
    case 1: // explicit euler
      y = y + dt*f(y);
      break;
    case 2: // symplectic euler
      y[1] = y[1] + dt*force(y)/m; //no damping here
      y[0] = y[0] + dt*y[1];
      break;
    case 3: // explicit midpoint
      y = y + dt * f(y+dt/2.*f(y));
      break;
    case 4: { 
      //y[2] = (y[2]*(m+d/m*dt)+dt*(k*(-y[1]-L)-g*m-d*y[2]))/(m+dt/m+dt*dt/m*k);
	  //y[1] += y[2]*dt;

	  // implicit euler
      Matrix2x2 A = Matrix2x2::Identity();
      A(0, 1) = -dt;
      A(1, 0) = 1.0*dt*k/m;
      A(1, 1) += dt*d/m;
      Vector2d b = y;
      b[1] += dt*(-1.0*k*L/m -g);
      y = A.inverse()*b;
      break;
    }
    case 5: { //analytic solution
    	const double alpha = -d / (2*m); 
    	const double beta = (std::sqrt(4*k*m - d*d))/(2*m);
    	const double c1 = (m*g)/k;
    	const double c2 = (m*g*alpha)/(k*beta);
    	y[0] = std::exp(alpha*(dt)) * (c1*cos(beta*(dt)) - c2*sin(beta*(dt))) - L - (m*g)/k;
    	y[1] = std::exp(alpha*(dt)) * (c1*alpha*cos(beta*(dt)) - c1*beta*sin(beta*(dt)) + c2*alpha*sin(beta*(dt)) - c2*beta*cos(beta*(dt)));
    	break;
    } 
    default: {
      assert(false || "Method not implemented yet");
      return;
    }
  }
  

  p2 = y[0];
  v2 = y[1];
}


/* Exercise 3
 * falling triangle
 * param k : spring stiffness
 * param m : spring mass
 * param d : spring damping
 * param L : spring rest length
 * param dt : timestep
 * param p1 : position 1
 * param p2 : position 2
 * param p3 : position 3
 * param v1 : velocity 1
 * param v2 : velocity 2
 * param v3 : velocity 3
 */
//using Vec2 = Vector2d;
void AdvanceTimeStep3(double k, double m, double d, double L, double dt,
                      Vec2& p1, Vec2& v1, Vec2& p2, Vec2& v2, Vec2& p3, Vec2& v3)
{
	//p1 += Vec2(1,1); - What is this ???
	#define POINT_NUM 3
	#define ZERO_VEC_ARRAY_3 Vec2(0.0,0.0)
	std::array<Vec2,POINT_NUM> forces{ZERO_VEC_ARRAY_3};
	std::array<Vec2,POINT_NUM> position{p1,p2,p3};
	std::array<Vec2,POINT_NUM> velocities{v1,v2,v3};
	int index = 0;
	
	//what does the index variable do here and why not just use i below?

	for(int i = 0; i < 3;++i) {
		// apply reaction force
		if(position[i].y <= -1.0) {
			position[i].y = -1.0;
			forces[index].y += m*g;
		}
		// gravity
		forces[index].y -= m*g;
		++index;
	}
	// velocities
	index = 0;
	for(int i = 0; i < 3;++i) {
		forces[index] -= d*velocities[i];
		++index;
	}
	// spring force
	for(int i = 0; i < 3;++i) {
		for(int j = 1; j <= 2; ++j) {
			// j-th spring force
			int i2 = (i + j) % 3;
			Vec2 delta = position[i] - position[i2];
			forces[i] -= k*(delta.length() - L)*delta.getNormalizedCopy();
		}
	}
	// apply symplectic euler
	std::array<Vec2,POINT_NUM> res_v{ZERO_VEC_ARRAY_3};
	std::array<Vec2,POINT_NUM> res_p{ZERO_VEC_ARRAY_3};
	for(int i = 0; i < 3;++i) {
		res_v[i] = velocities[i] + (dt/m)*forces[i];
		res_p[i] = position[i] + dt*res_v[i];
	}
	// apply to input
	p1 = res_p[0];
	p2 = res_p[1];
	p3 = res_p[2];
	
	v1 = res_v[0];
	v2 = res_v[1];
	v3 = res_v[2];
}

